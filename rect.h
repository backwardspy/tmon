#ifndef RECT_H
#define RECT_H

#include "vec2.h"

struct rect {
  struct vec2 origin, size, halfsize, center;
};

struct rect rect_new(struct vec2 origin, struct vec2 size);
bool rect_contains(struct rect rect, struct vec2 v);

#endif
