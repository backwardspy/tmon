#ifndef TILE_H
#define TILE_H

#include "common.h"

#define TILEFLAG_WALKABLE 1

struct tile {
  uint8_t flags;
  char ch;
  uint32_t fg, bg;
};

struct tile tile_new(uint8_t flags, char ch, uint32_t fg, uint32_t bg);

#endif
