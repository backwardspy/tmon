#include "tile.h"

struct tile tile_new(uint8_t flags, char ch, uint32_t fg, uint32_t bg) {
  struct tile t = {
    .flags = flags,
    .ch = ch,
    .fg = fg,
    .bg = bg,
  };
  return t;
}
