#include "action.h"
#include "actor.h"
#include "game.h"
#include "map.h"
#include "tile.h"

struct action action_new_walk(struct vec2 direction) {
  struct action act = {
    .type = ACTIONTYPE_WALK,
    .direction = direction,
  };
  return act;
}

struct action action_new_none() {
  struct action act = {
    .type = ACTIONTYPE_NONE,
  };
  return act;
}

void action_sprint(char *buf, const struct action *const act) {
  sprintf(buf,
          "action(type=%s, direction=(%d, %d))",
          ACTIONTYPE_NAMES[act->type],
          act->direction.x,
          act->direction.y);
}

struct action_result action_perform_none() {
  struct action_result result = {
    .success = false,
    .next_action = action_new_none(),
  };
  return result;
}

struct action_result action_perform_walk(const struct action *const act, struct actor *const a) {
  struct action_result result = {
    .success = false,
    .next_action = action_new_none(),
  };

  struct vec2 target_position = vec2_add(a->position, act->direction);
  const struct tile *target = map_get(game_state.map, target_position);
  if (target->flags & TILEFLAG_WALKABLE) {
    a->position = target_position;
    result.success = true;
  }

  return result;
}

struct action_result action_perform(const struct action *const act, struct actor *const a) {
  switch (act->type) {
  case ACTIONTYPE_WALK:
    return action_perform_walk(act, a);
  default:
    return action_perform_none();
  }
}
