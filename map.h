#ifndef MAP_H
#define MAP_H

#include "common.h"
#include "tile.h"
#include "vec2.h"

struct map {
  struct vec2 size;
  struct tile *tiles;
  struct tile void_tile;
};

struct map map_new(struct vec2 size);
const struct tile *map_get(struct map * map, struct vec2 position);
void map_render(struct map *map, struct vec2 offset);
void map_free(struct map *map);

#endif
