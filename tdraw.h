#ifndef TDRAW_H
#define TDRAW_H

#include "common.h"

struct rect;

void hline(struct tb_cell *buf, int32_t x0, int32_t x1, int32_t y);
void vline(struct tb_cell *buf, int32_t y0, int32_t y1, int32_t x);
void outline_rect(const struct rect *const r);

#endif
