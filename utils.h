#ifndef UTILS_H
#define UTILS_H

#include "common.h"

int32_t clamp(int32_t v, int32_t min, int32_t max);

#endif
