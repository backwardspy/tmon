#include "rect.h"

struct rect rect_new(struct vec2 origin, struct vec2 size) {
  struct rect r = {
    .origin = origin,
    .size = size,
    .halfsize = vec2_div(size, 2),
    .center = vec2_add(origin, vec2_div(size, 2)),
  };
  return r;
}

bool rect_contains(struct rect rect, struct vec2 v) {
  return v.x > rect.origin.x && v.y > rect.origin.y && v.x < (rect.origin.x + rect.size.x) && v.y < (rect.origin.y + rect.size.y);
}

