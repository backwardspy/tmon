#include "vec2.h"

struct vec2 vec2_new(int32_t x, int32_t y) {
  struct vec2 result = {
    .x = x,
    .y = y,
  };
  return result;
}

struct vec2 vec2_zero() {
  struct vec2 result = {
    .x = 0,
    .y = 0,
  };
  return result;
}

struct vec2 vec2_one() {
  struct vec2 result = {
    .x = 1,
    .y = 1,
  };
  return result;
}

struct vec2 vec2_add(struct vec2 a, struct vec2 b) {
  struct vec2 result = {
    .x = a.x + b.x,
    .y = a.y + b.y,
  };
  return result;
}

struct vec2 vec2_sub(struct vec2 a, struct vec2 b) {
  struct vec2 result = {
    .x = a.x - b.x,
    .y = a.y - b.y,
  };
  return result;
}

struct vec2 vec2_div(struct vec2 v, int32_t s) {
  struct vec2 result = {
    .x = v.x / s,
    .y = v.y / s,
  };
  return result;
}

struct vec2 vec2_neg(struct vec2 v) {
  struct vec2 result = {
    .x = -v.x,
    .y = -v.y,
  };
  return result;
}
