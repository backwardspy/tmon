#include "map.h"
#include "game.h"
#include "utils.h"

struct map map_new(struct vec2 size) {
  struct map map = {
    .size = size,
  };

  const char *floor = "             .,\"';:";
  const size_t floor_n = strlen(floor);

  map.tiles = (struct tile *)calloc(size.x * size.y, sizeof(struct tile));
  for (int32_t y = 0; y < size.y; y++) {
    int32_t row = y * size.x;
    for (int32_t x = 0; x < size.x; x++) {
      uint8_t flags = 0;
      bool edge = x == 0 || x == size.x - 1 || y == 0 || y == size.y - 1;
      if (!edge) {
        flags |= TILEFLAG_WALKABLE;
      }
      char ch = edge ? '#' : floor[rand() % floor_n];
      map.tiles[row + x] = tile_new(flags, ch, edge ? TB_YELLOW : TB_GREEN, TB_DEFAULT);
    }
  }

  map.void_tile = tile_new(0, '.', TB_BLACK, TB_DEFAULT);

  return map;
}

const struct tile *map_get(struct map *map, struct vec2 p) {
  if (p.x < 0 || p.x >= map->size.x || p.y < 0 || p.y >= map->size.y) {
    return &map->void_tile;
  }
  return &map->tiles[p.x + p.y * map->size.x];
}

void map_render(struct map *map, struct vec2 cam) {
  struct tb_cell *buf = tb_cell_buffer();

  struct vec2 morg = vec2_sub(cam, game_state.vp.halfsize);

  int32_t rw = clamp(game_state.vp.size.x, 0, game_state.display.cols);
  int32_t rh = clamp(game_state.vp.size.y, 0, game_state.display.rows);

  for (int32_t y = 0; y < rh; y++) {
    int32_t ry = game_state.vp.origin.y + y;
    int32_t row = ry * game_state.display.cols;
    for (int32_t x = 0; x < rw; x++) {
      int32_t rx = game_state.vp.origin.x + x;
      int32_t idx = row + rx;

      const struct tile * const tile = map_get(map, vec2_add(morg, vec2_new(x, y)));
      buf[idx].ch = tile->ch;
      buf[idx].fg = tile->fg;
      buf[idx].bg = tile->bg;
    }
  }
}

void map_free(struct map *map) {
  free(map->tiles);
  map->tiles = 0;
  map->size = vec2_zero();
}
