#include "actor.h"
#include "game.h"

struct actor actor_new(struct vec2 position, char ch, uint32_t fg, uint32_t bg) {
  struct actor a = {
    .position = position,
    .ch = ch,
    .fg = fg,
    .bg = bg,
    .next_action = action_new_none(),
  };

  for (stat_t stat = 0; stat <= STATTYPES_MAX; stat++) {
    a.stats[stat] = 0;
  }

  return a;
}

void actor_render(struct actor *a, struct vec2 cam) {
  struct vec2 p = vec2_add(vec2_sub(a->position, cam), game_state.vp.center);
  tb_change_cell(p.x, p.y, a->ch, a->fg, a->bg);
}
