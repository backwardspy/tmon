#ifndef GAME_H
#define GAME_H

#include "common.h"
#include "rect.h"

struct map;
struct actor;

struct display {
  int32_t rows, cols;
};

struct game_state {
  struct map *map;
  struct actor *hero;
  struct display display;
  struct rect vp;
};

extern struct game_state game_state;

void game_init();
void game_shutdown();

#endif
