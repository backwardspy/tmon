#ifndef VEC2_H
#define VEC2_H

#include "common.h"

struct vec2 {
  union {
    struct {
      int32_t x;
      int32_t y;
    };
    int32_t v[2];
  };
};

struct vec2 vec2_new(int32_t x, int32_t y);
struct vec2 vec2_zero();
struct vec2 vec2_one();

struct vec2 vec2_add(struct vec2 a, struct vec2 b);
struct vec2 vec2_sub(struct vec2 a, struct vec2 b);

struct vec2 vec2_div(struct vec2 v, int32_t s);

struct vec2 vec2_neg(struct vec2 v);

#endif
