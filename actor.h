#ifndef ACTOR_H
#define ACTOR_H

#include "common.h"
#include "vec2.h"
#include "action.h"

typedef size_t stat_t;
typedef uint8_t statv_t;

#define STATTYPES \
  X(0, STR, 'strength') \
  X(1, CON, 'constitution')                     \
  X(2, DEX, 'dexterity') \
  X(3, CHR, 'charisma')     \
  X(4, INT, 'intelligence') \
  X(5, WIS, 'wisdom') \
  X(6, HP, 'health points') \
  X(7, MP, 'mana points')

static const stat_t STATTYPES_MAX = 7;

#define X(id, name, desc) static const stat_t STATTYPE_##name = id;
STATTYPES
#undef X

static const char *STATTYPE_NAMES[] = {
#define X(id, name, desc) #name,
  STATTYPES
#undef X
};

struct actor {
  struct vec2 position;
  char ch;
  uint32_t fg, bg;
  struct action next_action;

  statv_t stats[8];
};

struct actor actor_new(struct vec2 position, char ch, uint32_t fg, uint32_t bg);
void actor_render(struct actor *a, struct vec2 offset);

#endif
