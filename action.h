#ifndef ACTION_H
#define ACTION_H

#include "common.h"
#include "vec2.h"

#define ACTIONTYPES \
  X(NONE, 0) \
  X(WALK, 1)

#define X(name, id) static const uint32_t ACTIONTYPE_##name = id;
ACTIONTYPES
#undef X

static const char *ACTIONTYPE_NAMES[] = {
#define X(name, id) #name,
  ACTIONTYPES
#undef X
};

struct actor;

struct action {
  uint32_t type;
  struct vec2 direction;
};

struct action_result {
  bool success;
  struct action next_action;
};

struct action action_new_walk(struct vec2 direction);
struct action action_new_none();

struct action_result action_perform(const struct action *const act, struct actor *const a);

void action_sprint(char *buf, const struct action *const act);

#endif
