#include "game.h"
#include "map.h"
#include "actor.h"

struct map map;
struct actor hero;

struct game_state game_state;

void game_init() {
  map = map_new(vec2_new(128, 64));
  hero = actor_new(vec2_new(64, 32), '@', TB_CYAN | TB_BOLD, TB_BLUE);
  game_state.map = &map;
  game_state.hero = &hero;
}

void game_shutdown() {
  map_free(game_state.map);
}
