#include "game.h"
#include "actor.h"
#include "tile.h"
#include "map.h"
#include "tdraw.h"

static const uint32_t STEP_RATE_NS = 10000000;

static const int32_t MIN_TERM_COLS = 80;
static const int32_t MIN_TERM_ROWS = 24;

static const uint32_t PANEL_WIDTH = 40;

uint32_t tbx_print(int32_t x, int32_t y, const char *str, uint16_t fg, uint16_t bg) {
  struct tb_cell *buf = tb_cell_buffer();

#ifndef NDEBUG
  if (x < 0 || x >= game_state.display.cols || y < 0 || y >= game_state.display.rows) {
    return -1;
  }
#endif

  uint32_t base = x + y * game_state.display.cols;
  size_t i = 0;
  while (str[i]) {
    buf[base + i].ch = str[i];
    buf[base + i].fg = fg;
    buf[base + i].bg = bg;
    i++;
  }

  return 0;
}

void nsleep(uint64_t nsec) {
  struct timespec req = {
    .tv_sec = 0,
    .tv_nsec = nsec,
  };
  while (nanosleep(&req, &req) != 0);
}

struct rect make_vp() {
  return rect_new(vec2_new(PANEL_WIDTH + 1, 2),
                  vec2_new(game_state.display.cols - PANEL_WIDTH - 2, game_state.display.rows - 3));
}

#ifndef NDEBUG
void dump_frame() {
  struct tb_cell *buf = tb_cell_buffer();
  FILE *f = fopen("frame.txt", "w");
  for (int32_t y = 0; y < game_state.display.rows; y++) {
    int32_t row = y * game_state.display.cols;
    for (int32_t x = 0; x < game_state.display.cols; x++) {
      fwrite(&buf[row + x].ch, sizeof(char), 1, f);
    }
    fwrite("\n", sizeof(char), 1, f);
  }
  fclose(f);
}
#else
#define dump_frame()
#endif

void init() {
  game_init();
  tb_init();
  tb_clear();
}

void deinit() {
  tb_shutdown();
  game_shutdown();
}

void resize(int32_t w, int32_t h) {
  game_state.display.cols = w;
  game_state.display.rows = h;
  game_state.vp = make_vp();
}

void clear() {
  tb_clear();
  resize(tb_width(), tb_height());
}

void present() {
  tb_present();
  resize(tb_width(), tb_height());
}

int main(void) {
  srand(time(0));

  init();

  int32_t term_cols = tb_width();
  int32_t term_rows = tb_height();
  if (term_cols < MIN_TERM_COLS || term_rows < MIN_TERM_ROWS) {
    deinit();
    printf("minimum supported terminal size is %dx%d. current size: %dx%d.\n", MIN_TERM_COLS, MIN_TERM_ROWS, term_cols, term_rows);
    return -1;
  }

  bool running = true;
  while (running) {
    struct tb_event ev;
    while (tb_peek_event(&ev, 0) > 0) {
      switch (ev.type) {
      case TB_EVENT_KEY:
        if (ev.ch == 'q') {
          running = false;
        } else if (ev.key == TB_KEY_ARROW_UP) {
          game_state.hero->next_action = action_new_walk(vec2_new(0, -1));
        } else if (ev.key == TB_KEY_ARROW_DOWN) {
          game_state.hero->next_action = action_new_walk(vec2_new(0, 1));
        } else if (ev.key == TB_KEY_ARROW_LEFT) {
          game_state.hero->next_action = action_new_walk(vec2_new(-1, 0));
        } else if (ev.key == TB_KEY_ARROW_RIGHT) {
          game_state.hero->next_action = action_new_walk(vec2_new(1, 0));
        }
        break;
      }
    }

    struct action_result action_result = action_perform(&game_state.hero->next_action, game_state.hero);
    if (action_result.success) {
      game_state.hero->next_action = action_result.next_action;
    }

    clear();

    struct vec2 cam = game_state.hero->position;

    hline(tb_cell_buffer(), 0, PANEL_WIDTH - 1, 1);
    outline_rect(&game_state.vp);

    map_render(game_state.map, cam);
    actor_render(game_state.hero, cam);

    char campos[128] = "";
    sprintf(campos, "cam: %d, %d", cam.x, cam.y);
    tbx_print(0, 2, campos, TB_CYAN | TB_BOLD, TB_DEFAULT);

    char stat_str[4] = "";
    for (stat_t stat = 0; stat <= STATTYPES_MAX; stat += 2) {
      int32_t ypos = 3 + stat / 2;

      tbx_print(0, ypos, STATTYPE_NAMES[stat], TB_GREEN | TB_BOLD, TB_DEFAULT);
      sprintf(stat_str, "%d", game_state.hero->stats[stat]);
      tbx_print(4, ypos, stat_str, TB_DEFAULT, TB_DEFAULT);

      tbx_print(8, ypos, STATTYPE_NAMES[stat + 1], TB_GREEN | TB_BOLD, TB_DEFAULT);
      sprintf(stat_str, "%d", game_state.hero->stats[stat + 1]);
      tbx_print(12, ypos, stat_str, TB_DEFAULT, TB_DEFAULT);
    }

    present();

    dump_frame();
    nsleep(STEP_RATE_NS);
  }

  deinit();

  return 0;
}
