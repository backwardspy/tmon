#include "tdraw.h"
#include "game.h"

void hline(struct tb_cell *buf, int32_t x0, int32_t x1, int32_t y) {
  int32_t base = y * game_state.display.cols;
  for (int32_t x = x0; x <= x1; x++) {
    buf[base + x].ch = '-';
    buf[base + x].fg = TB_BLACK | TB_BOLD;
    buf[base + x].bg = TB_BLACK;
  }
}

void vline(struct tb_cell *buf, int32_t y0, int32_t y1, int32_t x) {
  for (int32_t y = y0; y <= y1; y++) {
    int32_t idx = y * game_state.display.cols + x;
    buf[idx].ch = '|';
    buf[idx].fg = TB_BLACK | TB_BOLD;
    buf[idx].bg = TB_BLACK;
  }
}

void outline_rect(const struct rect *const r) {
  struct tb_cell *buf = tb_cell_buffer();
  int32_t top = r->origin.y - 1;
  int32_t bottom = r->origin.y + r->size.y;
  int32_t left = r->origin.x - 1;
  int32_t right = r->origin.x + r->size.x;
  hline(buf, left, right, top);
  hline(buf, left, right, bottom);
  vline(buf, top, bottom, left);
  vline(buf, top, bottom, right);
}
